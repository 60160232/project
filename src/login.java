import java.util.Scanner;

public class login {
	private static String username;
    private static String password;
	
    public static void showLogin(){
        System.out.println("Login");
    }
    
	public static void inputLogin(){
        Scanner kb = new Scanner(System.in);
        
        System.out.print("username : ");
        String username = kb.nextLine();
        System.out.print("password : ");
        String password = kb.nextLine();
    }
	
	private static boolean isUsernameCorrect(String username) {
        return true;
    }
    private static boolean isPasswordCorrect(String password) {
        return true;
    }
    
    private static void showLoginError() {
        System.out.println("username or password incorrect");           
    }
	
	public static void main(String[] args) {
		login();

	}
	
	private static void login() {
        while(true){
            showLogin();
            inputLogin();
            if(!isUsernameCorrect(username)){
                showLoginError();
                continue;
            }
            if(!isPasswordCorrect(password)){
                showLoginError();
                continue;
            }
            break;
        }
    }

}
